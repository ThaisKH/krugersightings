package com.itb.kruger.ui.home;

import android.app.ProgressDialog;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;


import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.itb.kruger.R;
import com.itb.kruger.model.Photo;

import java.util.List;

public class HomeFragment extends Fragment implements OnMapReadyCallback {

    private static final int REQUEST_ID_MULTIPLE_PERMISSIONS= 7;

    private GoogleMap map;
    private LiveData<List<Photo>> photos;
    private HomeViewModel homeViewModel;
    private LatLng locat;
    private FloatingActionButton fabCamera;
    private double lat = 0.0;
    private double lon = 0.0;
    private ProgressDialog pd;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        fabCamera = view.findViewById(R.id.fabCamera);
        if( getArguments() != null) {
            lat = getArguments().getDouble("Latitude");
            lon = getArguments().getDouble("Longitude");
        }
        SupportMapFragment fragment = (SupportMapFragment)getChildFragmentManager().findFragmentById(R.id.map);
        fragment.getMapAsync(this);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        fabCamera.setOnClickListener(this::onFABCameraClicked);
        pd = new ProgressDialog(getContext());
        photos = homeViewModel.getAllPhotos(pd);



        LiveData<Boolean> permissions = homeViewModel.checkPermissions(getActivity(), REQUEST_ID_MULTIPLE_PERMISSIONS);
        permissions.observe(getViewLifecycleOwner(), this::onPermissionsChanged);
    }

    private void onPermissionsChanged(Boolean hasPermissions) throws SecurityException{
        if(hasPermissions){
            if(lat != 0.0 && lon != 0.0){
                locat = new LatLng(lat,lon);
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(locat, 13));
            } else {
                positionCameraByLocation();
            }
            photos.observe(getViewLifecycleOwner(),this::onPhotoChange);
        }
    }

    private void onFABCameraClicked(View view) {
        Navigation.findNavController(view).navigate(R.id.HomeToCamera);
    }

    private void onPhotoChange(List<Photo> photos) throws SecurityException {
        LatLngBounds.Builder build = new LatLngBounds.Builder();
        Float color = null;
        for (Photo p : photos) {
            if (p.getAnimal() != null) {

                LatLng marker = new LatLng(p.getLatitude(), p.getLongitude());

                if (p.getAnimal().equalsIgnoreCase("Lion")) {
                    color = BitmapDescriptorFactory.HUE_RED;
                } else if (p.getAnimal().equalsIgnoreCase("Leopard")) {
                    color = BitmapDescriptorFactory.HUE_MAGENTA;
                } else if (p.getAnimal().equalsIgnoreCase("Buffalo")) {
                    color = BitmapDescriptorFactory.HUE_GREEN;
                } else if (p.getAnimal().equalsIgnoreCase("Elephant")) {
                    color = BitmapDescriptorFactory.HUE_BLUE;
                } else if (p.getAnimal().equalsIgnoreCase("Rhino")) {
                    color = BitmapDescriptorFactory.HUE_ORANGE;
                } else if (p.getAnimal().equalsIgnoreCase("Cheetah")) {
                    color = BitmapDescriptorFactory.HUE_VIOLET;
                } else if (p.getAnimal().equalsIgnoreCase("Wilddog")) {
                    color = BitmapDescriptorFactory.HUE_YELLOW;
                }

                map.addMarker(new MarkerOptions().title(p.getAnimal()).position(marker).icon(BitmapDescriptorFactory.defaultMarker(color)));
                build.include(marker);
            }
        }
    }

    public void positionCameraByLocation() throws SecurityException{
        LocationManager lm = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (location != null) {
            locat = new LatLng(location.getLatitude(), location.getLongitude());
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(locat, 13));
        } else {
            location = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            locat = new LatLng(location.getLatitude(), location.getLongitude());
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(locat, 13));
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap){
        map = googleMap;
        map.clear();
        map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
//        if(lat != 0.0 && lon != 0.0){
//            locat = new LatLng(lat,lon);
//            map.moveCamera(CameraUpdateFactory.newLatLngZoom(locat, 13));
//        } else {
//            positionCameraByLocation();
//        }
//        photos.observe(getViewLifecycleOwner(),this::onPhotoChange);
    }

}