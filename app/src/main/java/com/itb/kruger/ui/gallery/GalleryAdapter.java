package com.itb.kruger.ui.gallery;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.itb.kruger.R;
import com.itb.kruger.model.Photo;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.GalleryViewHolder> {
    private List<Photo> photos;
    private OnPhotoClickListener onPhotoClickListener;

    public GalleryAdapter() {
    }

    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
        notifyDataSetChanged();
    }

    public void setOnPhotoClickListener(OnPhotoClickListener onPhotoClickListener) {
        this.onPhotoClickListener = onPhotoClickListener;
    }

    @NonNull
    @Override
    public GalleryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.gallery_item, parent, false);
        return new GalleryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GalleryViewHolder holder, int position) {
        Photo photo = photos.get(position);
        Picasso.get().load(photo.getUri()).fit().into(holder.ivPhoto);
    }

    @Override
    public int getItemCount() {
        int size = 0;
        if(photos != null) size = photos.size();
        return size;
    }

    public class GalleryViewHolder extends RecyclerView.ViewHolder{
        ImageView ivPhoto;

        public GalleryViewHolder(@NonNull View view) {
            super(view);

            ivPhoto = view.findViewById(R.id.ivPhoto);
            ivPhoto.setOnClickListener(this::onPhotoClicked);
        }

        public void onPhotoClicked(View view){
            Photo photo = photos.get(getAdapterPosition());
            onPhotoClickListener.onPhotoClicked(photo);
        }
    }

    public interface OnPhotoClickListener{
        void onPhotoClicked(Photo photo);
    }
}
