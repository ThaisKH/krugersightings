package com.itb.kruger.repository;

import android.Manifest;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.ListResult;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.itb.kruger.model.Photo;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class Repository {
    private Application application;
    private StorageReference mStorageRef;
    private Uri photoUri;
    private String photoPath;

    public Repository(Application application) {
        this.application = application;
        this.mStorageRef = FirebaseStorage.getInstance().getReference();
    }

    public LiveData<List<Photo>> getAllPhotos(ProgressDialog pd){
        MutableLiveData<List<Photo>> photosLD = new MutableLiveData<>();
        List<Photo> photos = new ArrayList<>();
        pd.setTitle("Retrieving photos");
        pd.show();
        mStorageRef.listAll().addOnSuccessListener(new OnSuccessListener<ListResult>() {
            @Override
            public void onSuccess(ListResult listResult) {
                for (StorageReference sr: listResult.getItems()) {
                    Photo p = new Photo();
                    sr.getMetadata().addOnSuccessListener(new OnSuccessListener<StorageMetadata>() {
                        @Override
                        public void onSuccess(StorageMetadata storageMetadata) {
                            String animal = storageMetadata.getCustomMetadata("animal");
                            double lat = Double.parseDouble(storageMetadata.getCustomMetadata("lat"));
                            double lon = Double.parseDouble(storageMetadata.getCustomMetadata("long"));
                            p.setAnimal(animal);
                            p.setLatitude(lat);
                            p.setLongitude(lon);
                        }
                    });
                    sr.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            p.setUri(uri);
                            photos.add(p);
                            photosLD.postValue(photos);
                        }
                    });
                }
                pd.dismiss();
            }
        });
        return photosLD;
    }

    public LiveData<List<Photo>> getPhotosByAnimal(String animal, ProgressDialog pd){
        MutableLiveData<List<Photo>> photosLD = new MutableLiveData<>();
        List<Photo> photos = new ArrayList<>();
        pd.setTitle("Retrieving photos");
        pd.show();
        mStorageRef.listAll().addOnSuccessListener(new OnSuccessListener<ListResult>() {
            @Override
            public void onSuccess(ListResult listResult) {
                for (StorageReference sr: listResult.getItems()) {
                    Photo p = new Photo();
                    sr.getMetadata().addOnSuccessListener(new OnSuccessListener<StorageMetadata>() {
                        @Override
                        public void onSuccess(StorageMetadata storageMetadata) {
                            String anim = storageMetadata.getCustomMetadata("animal");
                            double lat = Double.parseDouble(Objects.requireNonNull(storageMetadata.getCustomMetadata("lat")));
                            double lon = Double.parseDouble(Objects.requireNonNull(storageMetadata.getCustomMetadata("long")));
                            p.setAnimal(anim);
                            p.setLatitude(lat);
                            p.setLongitude(lon);
                        }
                    });
                    sr.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            p.setUri(uri);
                            if(p.getAnimal()!=null){
                                if(p.getAnimal().equalsIgnoreCase(animal)){
                                    photos.add(p);
                                }
                            }
                            photosLD.postValue(photos);
                        }
                    });
                }
                pd.dismiss();
            }
        });
        return photosLD;
    }

    public String getPhotoPath(){
        return photoPath;
    }

    public void dispatchTakePictureIntent(Fragment fragment, int takePhotoCode) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(application.getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
            }
            if (photoFile != null) {
                photoUri = FileProvider.getUriForFile(application.getApplicationContext(),
                        "com.itb.kruger.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                fragment.startActivityForResult(takePictureIntent, takePhotoCode);
            }
        }
    }

    public void putFileWithMetadata(Location location, Spinner spinner, Context context, ProgressDialog pd) {
        StorageReference ref = mStorageRef.child(System.currentTimeMillis() +".jpg");
        StorageMetadata metadataImage = new StorageMetadata.Builder()
                .setCustomMetadata("lat",  location.getLatitude() + "")
                .setCustomMetadata("long", location.getLongitude() +"")
                .setCustomMetadata("animal", spinner.getSelectedItem().toString())
                .build();
        pd.setTitle("Uploading");
        pd.show();
        ref.putFile(photoUri,metadataImage).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                pd.dismiss();
                Toast.makeText(context,"Uploaded",Toast.LENGTH_LONG).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                pd.dismiss();
                Toast.makeText(context,"Failed",Toast.LENGTH_LONG).show();
            }});
    }

    public File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = application.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, ".jpg",  storageDir);
        photoPath = image.getAbsolutePath();
        addPictureToGallery(photoPath);
        return image;
    }

    public void addPictureToGallery(String path) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://"+ Environment.getExternalStorageDirectory()));
        File f = new File(path);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        application.sendBroadcast(mediaScanIntent);
    }

    public LiveData<Boolean> checkAndRequestPermissions(Activity activity, int requestIdPermissions ) {
        MutableLiveData<Boolean> hasPermissions = new MutableLiveData<>();

        int camera = ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA);
        int wtite = ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int read = ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE);
        int internet = ContextCompat.checkSelfPermission(activity, Manifest.permission.INTERNET);
        int fineLocation = ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION);
        int coarseLocation = ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION);

        List<String> listPermissionsNeeded = new ArrayList<>();
        if (wtite != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (camera != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (read != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if(internet != PackageManager.PERMISSION_GRANTED){
            listPermissionsNeeded.add(Manifest.permission.INTERNET);
        }
        if(fineLocation != PackageManager.PERMISSION_GRANTED){
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if(coarseLocation != PackageManager.PERMISSION_GRANTED){
            listPermissionsNeeded.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(activity, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), requestIdPermissions);
            hasPermissions.postValue(false);
        } else {
            hasPermissions.postValue(true);
        }

        return hasPermissions;
    }
}
